﻿// HomeWork17.5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>


class Vector
{
public:
    Vector() : x(2), y(2), z(2)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << sqrt(x*x + y*y + z*z);
    }
private:
    double x;
    double y;
    double z;
};

int main()
{
    Vector v;
    v.Show();
}